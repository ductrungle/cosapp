from .singleton import Singleton
from .observer import Subject, Observer
from .visitor import Visitor, send as send_visitor
from wrapt.wrappers import ObjectProxy as Proxy
